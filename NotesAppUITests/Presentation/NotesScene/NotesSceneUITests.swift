//
//  NotesSceneUITests.swift
//  NotesAppUITests
//
//  Created by Marsudi Widodo on 01/11/21.
//

import XCTest

class NotesSceneUITests: XCTestCase {

    override func setUp() {
        continueAfterFailure = false
        XCUIApplication().launch()
    }
    
    func testAddNote_thenDeleteIt() {
        let app = XCUIApplication()
        
        let title = "Just title notes"
        let content = "Lorem ipsum dolor sit amet consectetur adipiscing elit"
        
        app.buttons["addNewNoteButton"].tap()
        app.textFields["noteTitle"].tap()
        app.textFields["noteTitle"].typeText(title)
        
        app.textViews["contentTextView"].tap()
        app.textViews["contentTextView"].typeText(content)
        
        app.otherElements["notesEditorMainView"].tap()
        
        // Keyboard shortcut COMMAND + SHIFT + K while simulator has focus
        // need to dismiss keyboard, image view below keyboard
        //_ = app.images["orangeImageView"].waitForExistence(timeout: 5)
        //app.images["orangeImageView"].tap()
        
        app.buttons["saveBarButtonItem"].tap()
        
        XCTAssertTrue(app.buttons["addNewNoteButton"].waitForExistence(timeout: 5))
        app.tables.cells.firstMatch.tap()
        
        XCTAssertTrue(app.buttons["deleteBarButtonItem"].waitForExistence(timeout: 5))
        app.buttons["deleteBarButtonItem"].tap()
        app.alerts.buttons["I think no"].tap()
        app.buttons["deleteBarButtonItem"].tap()
        app.alerts.buttons["Yes, I am sure"].tap()
        
        XCTAssertTrue(app.buttons["addNewNoteButton"].waitForExistence(timeout: 5))
    }
}
