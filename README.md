## Programming & Tools

- Swift 5 (Programming language)
- Xcode 13.1 (IDE)
- UIKit (Framework)
- Core Data (Persistence storage)

## Screenshot
I made recorded screen, you can check inside screenshot directory under root project

## Architecture
- Clean Layered Architecture and MVVM
- I made some layer
  - Data layer: Repositories Implementations + Persistence DB
  - Domain layer: Entities + Use Cases + Repositories Interfaces
  - Presentation layer: ViewModels + Views
- Flow
  - View call method from viewModel
  - then viewModel execute useCase at domain layer 
  - repository returs data from persistence storage at data layer
  - and flows back to View and displayed there
## Application
This application contains two activity (view controller)
1. Note List
In this save note is displayed using table view, every cell contains, title, content and created date also contain color type label for the note available color is blue, orange, green and red. You can add new note by clicking add button and then screen will move to new note editor. 
2. Note Editor
This activity can be accessed by clicking add button in note list and will create new state for adding new note or clicking item in note list and will create state to edit or delete these note. 
Just type the title in edit text and content in text view, in this editor you can also text formatting and styling like bold, italic, underlin, and make ordered list or unordered list
The text view editor is using custom UITextview and NSTextStorage, proccessing some text formatting and styling is performed in custom NSTextStorage that delegate to custom UITextView. also I made a Toolbar text formatting in top of keyboard and tooltip, you can use this toolbar button / tooltip (menu controller) to formatting and styling text.

## Test
I only made some unit Test to perform ui testing, from add new note, save note until delete note.

## How to run
- clone this repository https://gitlab.com/mrsdwdd/notesapp
- move into project directory
- open `NotesApp.xcodeproj`
- and the run using xcode
