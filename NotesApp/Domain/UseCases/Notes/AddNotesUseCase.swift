//
//  AddNotesUseCase.swift
//  NotesApp
//
//  Created by Marsudi Widodo on 31/10/21.
//

import Foundation

final class AddNotesUseCase: UseCase {

    struct RequestValue {
        let note: Note
    }
    
    typealias ResultValue = (Result<Note, Error>)

    private let requestValue: RequestValue
    private let completion: (ResultValue) -> Void
    private let noteRepository: NoteRepository

    init(requestValue: RequestValue,
         completion: @escaping (ResultValue) -> Void,
         noteRepository: NoteRepository) {

        self.requestValue = requestValue
        self.completion = completion
        self.noteRepository = noteRepository
    }
    
    func save() {
        noteRepository.save(note: requestValue.note, completion: completion)
    }
}
