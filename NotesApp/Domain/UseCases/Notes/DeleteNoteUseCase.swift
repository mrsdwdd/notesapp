//
//  DeleteNoteUseCase.swift
//  NotesApp
//
//  Created by Marsudi Widodo on 01/11/21.
//

import Foundation

final class DeleteNoteUseCase: UseCase {

    struct RequestValue {
        let note: Note
    }
    
    typealias ResultValue = (Result<Void, Error>)

    private let requestValue: RequestValue
    private let completion: (ResultValue) -> Void
    private let noteRepository: NoteRepository

    init(requestValue: RequestValue,
         completion: @escaping (ResultValue) -> Void,
         noteRepository: NoteRepository) {

        self.requestValue = requestValue
        self.completion = completion
        self.noteRepository = noteRepository
    }
    
    func delete() {
        noteRepository.delete(note: requestValue.note, completion: completion)
    }
}
