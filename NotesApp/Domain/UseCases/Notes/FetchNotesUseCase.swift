//
//  FetchNotesUseCase.swift
//  NotesApp
//
//  Created by Marsudi Widodo on 31/10/21.
//

import Foundation

final class FetchNotesUseCase: UseCase {

    struct RequestValue {
        let maxCount: Int
    }
    
    typealias ResultValue = (Result<[Note], Error>)

    private let requestValue: RequestValue
    private let completion: (ResultValue) -> Void
    private let noteRepository: NoteRepository

    init(requestValue: RequestValue,
         completion: @escaping (ResultValue) -> Void,
         noteRepository: NoteRepository) {

        self.requestValue = requestValue
        self.completion = completion
        self.noteRepository = noteRepository
    }
    
    func fetch() {
        noteRepository.fetchAll(maxCout: requestValue.maxCount, completion: completion)
    }
}
