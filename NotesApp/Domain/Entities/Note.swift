//
//  Note.swift
//  NotesApp
//
//  Created by Marsudi Widodo on 31/10/21.
//

import Foundation

struct Note: Equatable {
    let id: UUID?
    let title: String?
    let content: String?
    let createdAt: Date?
    let color: String?
}
