//
//  UseCase.swift
//  NotesApp
//
//  Created by Marsudi Widodo on 31/10/21.
//

import Foundation

@objc
public protocol UseCase {
    @objc optional func fetch()
    @objc optional func save()
    @objc optional func update()
    @objc optional func delete()
}
