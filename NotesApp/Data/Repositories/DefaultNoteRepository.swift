//
//  DefaultNoteRepository.swift
//  NotesApp
//
//  Created by Marsudi Widodo on 31/10/21.
//

import Foundation

final class DefaultNoteRepository {
    
    private var notePersistentStorage: NotesStorage
    
    init(notePersistentStorage: NotesStorage) {
        self.notePersistentStorage = notePersistentStorage
    }
}

extension DefaultNoteRepository: NoteRepository {
    
    func fetchAll(maxCout: Int, completion: @escaping (Result<[Note], Error>) -> Void) {
        return notePersistentStorage.fetch(maxCount: maxCout, completion: completion)
    }
    
    func save(note: Note, completion: @escaping (Result<Note, Error>) -> Void) {
        notePersistentStorage.save(note: note, completion: completion)
    }
    
    func update(note: Note, completion: @escaping (Result<Note, Error>) -> Void) {
        notePersistentStorage.update(note: note, completion: completion)
    }
    
    func delete(note: Note, completion: @escaping (Result<Void, Error>) -> Void) {
        notePersistentStorage.delete(note: note, completion: completion)
    }
}
