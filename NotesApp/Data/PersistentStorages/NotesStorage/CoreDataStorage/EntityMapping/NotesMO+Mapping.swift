//
//  NotesMO+Mapping.swift
//  NotesApp
//
//  Created by Marsudi Widodo on 31/10/21.
//

import Foundation
import CoreData

extension NotesMO {
    convenience init(note: Note, insertInto context: NSManagedObjectContext) {
        self.init(context: context)
        title = note.title
        content = note.content
        createdAt = note.createdAt
        id = note.id
        color = note.color
    }
}

extension NotesMO {
    func toDomain() -> Note {
        return .init(id: id, title: title, content: content, createdAt: createdAt, color: color)
    }
}
