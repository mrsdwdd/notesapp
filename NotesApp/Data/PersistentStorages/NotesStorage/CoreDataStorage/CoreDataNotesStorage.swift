//
//  CoreDataNotesStorage.swift
//  NotesApp
//
//  Created by Marsudi Widodo on 31/10/21.
//

import Foundation
import CoreData

final class CoreDataNotesStorage {

    private let maxStorageLimit: Int
    private let coreDataStorage: CoreDataStorage

    init(maxStorageLimit: Int, coreDataStorage: CoreDataStorage = CoreDataStorage.shared) {
        self.maxStorageLimit = maxStorageLimit
        self.coreDataStorage = coreDataStorage
    }
}

extension CoreDataNotesStorage: NotesStorage {
    
    func fetch(maxCount: Int, completion: @escaping (Result<[Note], Error>) -> Void) {
        coreDataStorage.performBackgroundTask { context in
            do {
                let request: NSFetchRequest = NotesMO.fetchRequest()
                request.sortDescriptors = [NSSortDescriptor(key: #keyPath(NotesMO.createdAt), ascending: false)]
                request.fetchLimit = maxCount
                let result = try context.fetch(request).map { $0.toDomain() }
                completion(.success(result))
            } catch {
                completion(.failure(CoreDataStorageError.readError(error)))
            }
        }
    }
    
    func save(note: Note, completion: @escaping (Result<Note, Error>) -> Void) {
        coreDataStorage.performBackgroundTask { context in
            do {
                let entity = NotesMO(note: note, insertInto: context)
                try context.save()
                completion(.success(entity.toDomain()))
            } catch {
                completion(.failure(CoreDataStorageError.saveError(error)))
            }
        }
    }
    
    func update(note: Note, completion: @escaping (Result<Note, Error>) -> Void) {
        coreDataStorage.performBackgroundTask { context in
            guard let id = note.id else { return }
            do {
                let request: NSFetchRequest = NotesMO.fetchRequest()
                let query = NSPredicate(format: "%K == %@", "id", id as CVarArg)
                request.predicate = query
                request.fetchLimit = 1
                let result = try context.fetch(request)
                if !result.isEmpty {
                    let notesMO = result[0]
                    notesMO.title = note.title
                    notesMO.content = note.content
                    notesMO.color = note.color
                    try context.save()
                }
                
                completion(.success(result[0].toDomain()))
            } catch {
                completion(.failure(CoreDataStorageError.saveError(error)))
            }
        }
    }
    
    func delete(note: Note, completion: @escaping (Result<Void, Error>) -> Void) {
        coreDataStorage.performBackgroundTask { context in
            guard let id = note.id else { return }
            do {
                let request: NSFetchRequest = NotesMO.fetchRequest()
                let query = NSPredicate(format: "%K == %@", "id", id as CVarArg)
                request.predicate = query
                request.fetchLimit = 1
                
                let result = try context.fetch(request)
                result.forEach { notesMo in
                    context.delete(notesMo)
                }
                
                try context.save()
                
                completion(.success(()))
            } catch {
                completion(.failure(CoreDataStorageError.deleteError(error)))
            }
        }
    }
}
