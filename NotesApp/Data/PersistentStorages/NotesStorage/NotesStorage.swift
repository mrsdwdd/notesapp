//
//  NotesStorage.swift
//  NotesApp
//
//  Created by Marsudi Widodo on 31/10/21.
//

import Foundation

protocol NotesStorage {
    func fetch(maxCount: Int, completion: @escaping (Result<[Note], Error>) -> Void)
    func save(note: Note, completion: @escaping (Result<Note, Error>) -> Void)
    func update(note: Note, completion: @escaping (Result<Note, Error>) -> Void)
    func delete(note: Note, completion: @escaping (Result<Void, Error>) -> Void)
}
