//
//  AlphaTextStorage.swift
//  AlphaTextView
//
//  Created by Marsudi Widodo on 03/11/21.
//
//

import UIKit

protocol AlphaTextStorageDelegate {
    func replace(in range: NSRange, with str: String)
    func performReplacementsForRange(_ range: NSRange, font: UIFont)
}

class AlphaTextStorage: NSTextStorage {
    
    var backedString: NSMutableAttributedString = NSMutableAttributedString()
    
    var fontType: UIFont = FontType.shared.regular
    var isChangeCharacters: Bool = false
    var underline: Bool = false

    var _delegate: AlphaTextStorageDelegate!
    
    override var string: String {
        return backedString.string
    }

    override func attributes(at location: Int, effectiveRange range: NSRangePointer?) -> [NSAttributedString.Key : Any] {
        return backedString.attributes(at: location, effectiveRange: range)
    }

    override func replaceCharacters(in range: NSRange, with str: String) {
        _delegate.replace(in: range, with: str)
    }

    override func setAttributes(_ attrs: [NSAttributedString.Key : Any]?, range: NSRange) {
        guard backedString.string.count > range.location else {
            return
        }

        beginEditing()
        backedString.setAttributes(attrs, range: range)
        edited(.editedAttributes, range: range, changeInLength: 0)
        endEditing()
    }
    
    override func processEditing() {
        if isChangeCharacters && editedRange.length > 0 {
            isChangeCharacters = false
            _delegate.performReplacementsForRange(editedRange, font: fontType)
        }

        super.processEditing()
    }
    
    func safeReplaceCharactersInRange(_ range: NSRange, withString str: String) {
        if isSafeRange(range) {
            replaceCharacters(in: range, with: str)
        }
    }

    func safeReplaceCharactersInRange(_ range: NSRange, withAttributedString attrStr: NSAttributedString) {
        if isSafeRange(range) {
            replaceCharacters(in: range, with: attrStr)
        }
    }
    
    func safeAddAttributes(_ attrs: [NSAttributedString.Key : AnyObject], range: NSRange) {
        if isSafeRange(range) {
            addAttributes(attrs, range: range)
        }
    }
    
    func safeRemoveAttribute(_ attr: NSAttributedString.Key, range: NSRange) {
        if isSafeRange(range) {
            removeAttribute(attr, range: range)
        }
    }
    
    func safeAttribute(_ attrName: String, atIndex location: Int, effectiveRange range: NSRangePointer?, defaultValue: AnyObject?) -> AnyObject? {
        var attributeValue: AnyObject? = nil

        if location >= 0 && location < string.count {
            attributeValue = attribute(NSAttributedString.Key(rawValue: attrName), at: location, effectiveRange: range) as AnyObject?
        }

        return attributeValue == nil ? defaultValue : attributeValue
    }

    func isSafeRange(_ range: NSRange) -> Bool {
        if range.location < 0 {
            return false
        }

        let maxLength = range.location + range.length
        if maxLength <= string.count {
            return true
        } else {
            return false
        }
    }
}
