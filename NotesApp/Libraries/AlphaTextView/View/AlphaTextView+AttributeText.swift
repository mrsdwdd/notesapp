//
//  AlphaTextView+Parser.swift
//  AlphaTextView
//
//  Created by Marsudi Widodo on 03/11/21.
//

import Foundation
import UIKit

extension AlphaTextView {
    
    func textAttributesDataWithAttributedString(_ attributedString: NSAttributedString) -> [Dictionary<String, AnyObject>] {
        var attributesData: [Dictionary<String, AnyObject>] = []

        attributedString.enumerateAttributes(in: NSRange(location: 0, length: attributedString.string.count), options: .reverse) { (attr, range, mutablePointer) in
            attr.keys.forEach {
                var attribute = [String: AnyObject]()
                
                attribute["name"] = $0 as AnyObject?
                attribute["location"] = range.location as AnyObject?
                attribute["length"] = range.length as AnyObject?

                if $0 == NSAttributedString.Key.font {
                    let currentFont = attr[$0] as! UIFont

                    var ft = "regular"

                    if currentFont.isRegular {
                        ft = "regular"
                    } else if currentFont.isBold {
                        ft = "bold"
                    } else if currentFont.isItalic {
                        ft = "italic"
                    } else if currentFont.isBoldItalic {
                        ft = "boldItalic"
                    }
                    
                    attribute["type"] = ft as AnyObject?

                    attributesData.append(attribute)
                } else if $0 == NSAttributedString.Key.paragraphStyle {
                    let paragraphType = currentParagraphTypeWithLocation(range.location)

                    if paragraphType == .ol || paragraphType == .ul {
                        attribute["type"] = paragraphType.rawValue as AnyObject?
                        attributesData.append(attribute)
                    }
                } else if $0 == NSAttributedString.Key.underlineStyle {
                    attribute["underline"] = 1 as AnyObject?
                    attributesData.append(attribute)
                }
            }
        }

        return attributesData
    }
    
    func textAttributesJSON() -> String {
        let attributesData: [Dictionary<String, AnyObject>] = textAttributesDataWithAttributedString(attributedText)

        return jsonStringWithAttributesData(attributesData, text: text)
    }

    func setAttributeTextWithJSONString(_ jsonString: String) {
        
        guard let data = jsonString.data(using: .utf8) else {
            return
        }
        
        let jsonDict = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: AnyObject]
        let text = jsonDict?["text"] as? String
        
        self.attributedText = NSAttributedString(string: text ?? "", attributes: self.defaultAttributesForLoad)

        setAttributesWithJSONString(jsonString)
    }

    func setAttributesWithJSONString(_ jsonString: String) {
        let attributes = attributesWithJSONString(jsonString)
        let textString = NSString(string: textWithJSONString(jsonString))

        attributes.forEach {
            let attribute = $0
            let attributeName = attribute["name"] as! String
            let range = NSRange(location: attribute["location"] as! Int, length: attribute["length"] as! Int)

            if attributeName == NSAttributedString.Key.font.rawValue {
                let currentFont = fontOfTypeWithAttribute(attribute)
                textStorage.addAttribute(NSAttributedString.Key(rawValue: attributeName), value: currentFont, range: range)
            } else if attributeName == NSAttributedString.Key.paragraphStyle.rawValue {
                let listTypeRawValue = attribute["type"]

                if listTypeRawValue != nil {
                    let listType = ParagraphStyle(rawValue: listTypeRawValue as! String)
                    var listPrefixWidth: CGFloat = 0

                    if listType == .ol {
                        var listPrefixString = textString.substring(with: range).components(separatedBy: " ")[0]
                        listPrefixString.append(" ")
                        listPrefixWidth = NSString(string: listPrefixString).size(withAttributes: [NSAttributedString.Key.font: FontType.shared.regular]).width
                    } else {
                        listPrefixWidth = NSString(string: "• ").size(withAttributes: [NSAttributedString.Key.font: FontType.shared.regular]).width
                    }

                    let lineHeight = FontType.shared.regular.lineHeight

                    let paragraphStyle = defaultParagraphStyle()
                    paragraphStyle.headIndent = listPrefixWidth + lineHeight
                    paragraphStyle.firstLineHeadIndent = lineHeight
                    textStorage.addAttributes([NSAttributedString.Key.paragraphStyle: paragraphStyle, NSAttributedString.Key.font: FontType.shared.regular], range: range)
                }
            } else if attributeName == NSAttributedString.Key.underlineStyle.rawValue {
                textStorage.addAttributes([NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue], range: range)
            }
        }
    }

    func attributesWithJSONString(_ jsonString: String) -> [[String: AnyObject]] {
        
        guard let data = jsonString.data(using: .utf8) else {
            return []
        }
        
        let jsonDict = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: AnyObject]
        

        let attributes = jsonDict?["attributes"] as? [[String: AnyObject]]

        return attributes ?? []
    }

    func jsonStringWithAttributesData(_ attributesData: [Dictionary<String, AnyObject>], text currentText: String) -> String {
        var jsonDict: [String: AnyObject] = [:]

        jsonDict["text"] = currentText as AnyObject?
        jsonDict["attributes"] = attributesData as AnyObject?
        
        let jsonData = try! JSONSerialization.data(withJSONObject: jsonDict, options: .prettyPrinted)
        return String(data: jsonData, encoding: String.Encoding.utf8)!
    }

    func textWithJSONString(_ jsonString: String) -> String {
        
            
        guard let data = jsonString.data(using: .utf8) else {
            return ""
        }
        
        let jsonDict = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: AnyObject]

        let textString = jsonDict?["text"] as? String
        return textString ?? ""
    }
    
    func fontOfTypeWithAttribute(_ attribute: [String: AnyObject]) -> UIFont {
        let ft = attribute["type"] as? String
        var currentFont = FontType.shared.regular

        if ft == "regular" {
            currentFont = FontType.shared.regular
        } else if ft == "bold" {
            currentFont = FontType.shared.bold
        } else if ft == "italic" {
            currentFont = FontType.shared.italic
        } else if ft == "boldItalic" {
            currentFont = FontType.shared.boldItalic
        }
        return currentFont
    }
}
