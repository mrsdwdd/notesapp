//
//  AlphaTextView.swift
//  AlphaTextView
//
//  Created by Marsudi Widodo on 03/11/21.
//
//

import UIKit

class AlphaTextView: UITextView {
    
    enum ParagraphStyle: String {
        case `default`, ul, ol
    }
    
    var defaultAttributesForLoad: [NSAttributedString.Key : AnyObject] = [:]
    
    let storage = AlphaTextStorage()
    let ulRegex = try! NSRegularExpression(pattern: "^[-*••∙●] ", options: .caseInsensitive)
    let olRegex = try! NSRegularExpression(pattern: "^\\d*\\. ", options: .caseInsensitive)
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    init(frame: CGRect) {
        
        let textContainer = NSTextContainer()
        let layoutManager = NSLayoutManager()
        layoutManager.addTextContainer(textContainer)
        storage.addLayoutManager(layoutManager)
        
        super.init(frame: frame, textContainer: textContainer)
        
        delegate = self
        storage._delegate = self
        
        prepareMenuTooltip()
        prepareAccessoryView()
    }
    
    override func caretRect(for position: UITextPosition) -> CGRect {
        var originalRect = super.caretRect(for: position)
        originalRect.size.height = storage.fontType.lineHeight
        return originalRect
    }
}
