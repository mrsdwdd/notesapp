//
//  AlphaTextStorage+UndoManager.swift
//  AlphaTextView
//
//  Created by Marsudi Widodo on 03/11/21.
//

import UIKit

extension AlphaTextView {
    
    func saveCurrentStateAndRegisterForUndo(_ range: NSRange, toFont: UIFont, currentFont: UIFont) {
        undoManager?.registerUndo(withTarget: self, handler: { (type) in
            self.saveCurrentStateAndRegisterForUndo(range, toFont: toFont, currentFont: currentFont)
        })

        if undoManager!.isUndoing {
            performReplacementsForRange(range, font: currentFont)
        } else {
            performReplacementsForRange(range, font: toFont)
        }
    }

    func saveCurrentStateAndRegisterForUndo(_ replaceRange: NSRange, withAttributedString attributedStr: NSAttributedString, oldAttributedString: NSAttributedString, selectedRangeLocationMove: Int) {
        undoManager?.registerUndo(withTarget: self, handler: { (type) in
            self.saveCurrentStateAndRegisterForUndo(replaceRange, withAttributedString: attributedStr, oldAttributedString: oldAttributedString, selectedRangeLocationMove: selectedRangeLocationMove)
        })

        if undoManager!.isUndoing {
            let targetSelectedRange = NSRange(location: selectedRange.location - selectedRangeLocationMove, length: selectedRange.length)
            storage.safeReplaceCharactersInRange(NSRange(location: replaceRange.location, length: attributedStr.string.count), withAttributedString: oldAttributedString)
            selectedRange = targetSelectedRange
        } else {
            let targetSelectedRange = NSRange(location: selectedRange.location + selectedRangeLocationMove, length: selectedRange.length)
            storage.safeReplaceCharactersInRange(replaceRange, withAttributedString: attributedStr)
            selectedRange = targetSelectedRange
        }
    }

    func saveCurrentStateAndRegisterForUndo(_ replaceRange: NSRange, withString str: String, selectedRangeLocationMove: Int) {
        undoManager?.registerUndo(withTarget: self, handler: { (type) in
            self.saveCurrentStateAndRegisterForUndo(replaceRange, withString: str, selectedRangeLocationMove: selectedRangeLocationMove)
        })

        if undoManager!.isUndoing {
            selectedRange = NSRange(location: selectedRange.location - selectedRangeLocationMove, length: 0)
            storage.safeReplaceCharactersInRange(NSRange(location: replaceRange.location, length: str.count), withString: "")
        } else {
            storage.safeReplaceCharactersInRange(replaceRange, withString: str)
            selectedRange = NSRange(location: selectedRange.location + selectedRangeLocationMove, length: 0)
        }
    }

    func saveCurrentStateAndRegisterForUndo(_ replaceRange: NSRange, withAttributedString attributedStr: NSAttributedString, selectedRangeLocationMove: Int) {
        undoManager?.registerUndo(withTarget: self, handler: { (type) in
            self.saveCurrentStateAndRegisterForUndo(replaceRange, withAttributedString: attributedStr, selectedRangeLocationMove: selectedRangeLocationMove)
        })

        if undoManager!.isUndoing {
            selectedRange = NSRange(location: selectedRange.location - selectedRangeLocationMove, length: 0)
            storage.safeReplaceCharactersInRange(NSRange(location: replaceRange.location, length: attributedStr.string.count), withAttributedString: NSAttributedString())
        } else {
            storage.safeReplaceCharactersInRange(replaceRange, withAttributedString: attributedStr)
            selectedRange = NSRange(location: selectedRange.location + selectedRangeLocationMove, length: 0)
        }
    }

    func saveCurrentIndentAndRegisterForUndo(_ range: NSRange, headIndent: CGFloat) {
        undoManager?.registerUndo(withTarget: self, handler: { (type) in
            self.saveCurrentIndentAndRegisterForUndo(range, headIndent: headIndent)
        })

        let paragraphStyle = defaultParagraphStyle()

        if undoManager!.isUndoing {
            paragraphStyle.headIndent = 0
            paragraphStyle.firstLineHeadIndent = 0
        } else {
            paragraphStyle.headIndent = headIndent + FontType.shared.regular.lineHeight
            paragraphStyle.firstLineHeadIndent = FontType.shared.regular.lineHeight
        }

        storage.safeAddAttributes([NSAttributedString.Key.paragraphStyle : paragraphStyle], range: range)
    }

    func resetCurrentIndentAndRegisterForUndo(_ range: NSRange, headIndent: CGFloat) {
        undoManager?.registerUndo(withTarget: self, handler: { (type) in
            self.resetCurrentIndentAndRegisterForUndo(range, headIndent: headIndent)
        })

        let paragraphStyle = defaultParagraphStyle()

        if undoManager!.isUndoing {
            paragraphStyle.headIndent = headIndent + FontType.shared.regular.lineHeight
            paragraphStyle.firstLineHeadIndent = FontType.shared.regular.lineHeight
        } else {
            paragraphStyle.headIndent = 0
            paragraphStyle.firstLineHeadIndent = 0
        }

        storage.safeAddAttributes([NSAttributedString.Key.paragraphStyle : paragraphStyle], range: range)
    }
}
