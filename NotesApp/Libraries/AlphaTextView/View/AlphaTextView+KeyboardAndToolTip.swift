//
//  AlphaTextView+KeyboardAndToolTip.swift
//  AlphaTextView
//
//  Created by Marsudi Widodo on 03/11/21.
//

import Foundation
import UIKit

extension AlphaTextView {
    
    func prepareAccessoryView(){
        let accessaryView = UIView()
        let accessoryViewHeight: CGFloat = 50
        
        let boldButton = UIButton()
        let italicButton = UIButton()
        let underlineButton = UIButton()
        let orderedListButton = UIButton()
        let unOrderedListButton = UIButton()
        
        accessaryView.frame = .init(origin: .zero, size: CGSize(width: 10, height: accessoryViewHeight))
        accessaryView.backgroundColor = .systemGray5
        accessaryView.accessibilityIdentifier = "accessoryView"
        inputAccessoryView = accessaryView
        
        let config = UIImage.SymbolConfiguration(pointSize: 25, weight: .regular, scale: .small)
        
        let boldImage = UIImage(systemName: "bold", withConfiguration: config)
        let italicImage = UIImage(systemName: "italic", withConfiguration: config)
        let underlineImage = UIImage(systemName: "underline", withConfiguration: config)
        let orderedListImage = UIImage(systemName: "list.number", withConfiguration: config)
        let unOrderedListImage = UIImage(systemName: "list.bullet", withConfiguration: config)
        
        boldButton.setImage(boldImage, for: .normal)
        italicButton.setImage(italicImage, for: .normal)
        underlineButton.setImage(underlineImage, for: .normal)
        orderedListButton.setImage(orderedListImage, for: .normal)
        unOrderedListButton.setImage(unOrderedListImage, for: .normal)
        
        boldButton.tintColor = .systemGray
        italicButton.tintColor = .systemGray
        underlineButton.tintColor = .systemGray
        orderedListButton.tintColor = .systemGray
        unOrderedListButton.tintColor = .systemGray
        
        orderedListButton.tag = 1
        unOrderedListButton.tag = 2
        
        boldButton.addTarget(self, action: #selector(makeBold), for: .touchUpInside)
        italicButton.addTarget(self, action: #selector(makeItalic), for: .touchUpInside)
        underlineButton.addTarget(self, action: #selector(makeUnderline), for: .touchUpInside)
        orderedListButton.addTarget(self, action: #selector(makeTextStyle(_:)), for: .touchUpInside)
        unOrderedListButton.addTarget(self, action: #selector(makeTextStyle(_:)), for: .touchUpInside)
        
        let buttonStack = UIStackView(arrangedSubviews: [boldButton, italicButton, underlineButton, orderedListButton, unOrderedListButton])
        buttonStack.alignment = .fill
        buttonStack.distribution = .fillEqually
        buttonStack.spacing = 20
        
        accessaryView.addSubview(buttonStack)
        buttonStack.anchor(top: accessaryView.topAnchor, leading: accessaryView.safeAreaLayoutGuide.leadingAnchor, bottom: accessaryView.bottomAnchor, trailing: accessaryView.safeAreaLayoutGuide.trailingAnchor, padding: .init(top: 0, left: 20, bottom: 0, right: 20))
    }
    
    func prepareMenuTooltip() {
        let menuController = UIMenuController.shared
        var menuItems = [UIMenuItem]()
        
        menuItems.append(UIMenuItem(title: "Bold", action: #selector(self.makeBold)))
        menuItems.append(UIMenuItem(title: "Italic", action: #selector(self.makeItalic)))
        menuItems.append(UIMenuItem(title: "Underline", action: #selector(self.makeUnderline)))

        menuController.menuItems = menuItems
    }
    
    @objc func makeBold() {
        var toFont: UIFont =  FontType.shared.regular
        
        if storage.fontType.isBoldItalic {
            toFont = FontType.shared.italic
        } else if storage.fontType.isItalic {
            toFont = FontType.shared.boldItalic
        } else if storage.fontType.isBold {
            toFont = FontType.shared.regular
        } else if storage.fontType.isRegular {
            toFont = FontType.shared.bold
        }
        
        makeTextFormat(toFont)
    }
    
    @objc func makeItalic() {
        var toFont: UIFont =  FontType.shared.regular
        
        if storage.fontType.isBoldItalic {
            toFont = FontType.shared.bold
        } else if storage.fontType.isItalic {
            toFont = FontType.shared.regular
        } else if storage.fontType.isBold {
            toFont = FontType.shared.boldItalic
        } else if storage.fontType.isRegular {
            toFont = FontType.shared.italic
        }
        
        makeTextFormat(toFont)
    }
    
    @objc func makeUnderline() {
        storage.underline = !storage.underline
        makeTextFormat(storage.fontType)
    }
    
    func makeTextFormat(_ font: UIFont) {
        if isSelectedTextWithTextView(self) {
            changeSelectedTextWithFontType(with: font)
        } else {
            changeCurrentParagraphTextWithFontType(with: font)
        }
    }
    
    @objc func makeTextStyle(_ sender: UIButton) {
        if sender.tag == 1 {
            self.changeCurrentParagraphToOrderedList(orderedList: true, listPrefix: "1. ")
        } else {
            self.changeCurrentParagraphToOrderedList(orderedList: false, listPrefix: "• ")
        }
    }
}
