//
//  swift
//  AlphaTextView
//
//  Created by Marsudi Widodo on 03/11/21.
//
//

import UIKit

extension AlphaTextView {
    
    func isReturn(_ text: String) -> Bool {
        return text == "\n"
    }

    func isBackspace(_ text: String) -> Bool {
        return text == ""
    }

    func isSelectedTextWithTextView(_ textView: UITextView) -> Bool {
        let length = textView.selectedRange.length
        return length > 0
    }

    func objectLineAndIndexWithString(_ string: String, location: Int) -> (String, Int) {
        let str = NSString(string: string)

        var objectIndex: Int = 0
        var objectLine = str.substring(to: location)

        let textSplits = objectLine.components(separatedBy: "\n")
        if textSplits.count > 0 {
            let currentObjectLine = textSplits[textSplits.count - 1]

            objectIndex = objectLine.count - currentObjectLine.count
            objectLine = currentObjectLine
        }

        return (objectLine, objectIndex)
    }

    func objectLineWithString(_ string: String, location: Int) -> String {
        return objectLineAndIndexWithString(string, location: location).0
    }

    func lineEndIndexWithString(_ string: String, location: Int) -> Int {
        let remainText: NSString = NSString(string: string).substring(from: location) as NSString

        var nextLineBreakLocation = remainText.range(of: "\n").location
        nextLineBreakLocation = (nextLineBreakLocation == NSNotFound) ? string.count : nextLineBreakLocation + location

        return nextLineBreakLocation
    }

    func paragraphRangeOfString(_ string: String, location: Int) -> NSRange {
        let startLocation = objectLineAndIndexWithString(string, location: location).1
        let endLocation = lineEndIndexWithString(string, location: location)

        return NSRange(location: startLocation, length: endLocation - startLocation)
    }

    func currentParagraphStringOfString(_ string: String, location: Int) -> String {
        return NSString(string: string).substring(with: paragraphRangeOfString(string, location: location))
    }
    
    func paragraphTypeWithObjectLine(_ objectLine: String) -> AlphaTextView.ParagraphStyle {
        let objectLineRange = NSRange(location: 0, length: objectLine.count)

        let unorderedListMatches = ulRegex.matches(in: objectLine, options: [], range: objectLineRange)
        if unorderedListMatches.count > 0 {
            return .ul
        }

        let orderedListMatches = olRegex.matches(in: objectLine, options: [], range: objectLineRange)
        if orderedListMatches.count > 0 {
            return .ol
        }

        return .default
    }

    func isListParagraph(_ objectLine: String) -> Bool {
        let objectLineRange = NSRange(location: 0, length: objectLine.count)

        let isCurrentOrderedList = olRegex.matches(in: objectLine, options: [], range: objectLineRange).count > 0
        if isCurrentOrderedList {
            return true
        }

        let isCurrentUnorderedList = ulRegex.matches(in: objectLine, options: [], range: objectLineRange).count > 0
        if isCurrentUnorderedList {
            return true
        }

        return false
    }
    
    func changeCurrentParagraphTextWithFontType(with font: UIFont) {
        storage.fontType = font
        let paragraphRange = paragraphRangeOfString(self.text, location: selectedRange.location)
        let currentFont = currentFontWithIndex(paragraphRange.location)
        saveCurrentStateAndRegisterForUndo(paragraphRange, toFont: storage.fontType, currentFont: currentFont)
    }

    func changeSelectedTextWithFontType(with font: UIFont) {
        storage.fontType = font
        let currentFont = currentFontWithIndex(selectedRange.location)
        saveCurrentStateAndRegisterForUndo(selectedRange, toFont: storage.fontType, currentFont: currentFont)
    }
    
    func changeCurrentParagraphToOrderedList(orderedList isOrderedList: Bool, listPrefix: String) {
        
        var targetText: NSString!
        var targetRange: NSRange!

        let objectLineAndIndex = objectLineAndIndexWithString(self.text, location: selectedRange.location)
        let objectIndex = objectLineAndIndex.1

        if selectedRange.length == 0 {
            targetText = currentParagraphStringOfString(text, location: selectedRange.location) as NSString
            targetRange = NSRange(location: objectIndex, length: targetText.length)
        } else {
            var lastIndex = selectedRange.location + selectedRange.length
            lastIndex = lineEndIndexWithString(text, location: lastIndex)
            targetRange = NSRange(location: objectIndex, length: lastIndex - objectIndex)
            targetText = (text as NSString).substring(with: targetRange) as NSString
        }
        
        let objectLineRange = NSRange(location: 0, length: targetText.length)

        let isCurrentOrderedList = olRegex.matches(in: String(targetText), options: [], range: objectLineRange).count > 0
        let isCurrentUnorderedList = ulRegex.matches(in: String(targetText), options: [], range: objectLineRange).count > 0

        let isCurrentList = (isCurrentOrderedList || isCurrentUnorderedList)
        let isTransformToList = (isOrderedList && !isCurrentOrderedList) || (!isOrderedList && !isCurrentUnorderedList)

        var numberedIndex = 1
        var replacedContents: [NSString] = []
        
        targetText.enumerateLines { (line, stop) in
            var currentLine: NSString = line as NSString
            
            if self.isListParagraph(line) {
                currentLine = currentLine.substring(from: currentLine.range(of: " ").location + 1) as NSString
            }
            
            if isTransformToList {
                if isOrderedList {
                    currentLine = NSString(string: "\(numberedIndex). ").appending(String(currentLine)) as NSString
                    numberedIndex += 1
                } else {
                    currentLine = NSString(string: listPrefix).appending(String(currentLine)) as NSString
                }
            }

            replacedContents.append(currentLine)
        }

        var replacedContent = NSArray(array: replacedContents).componentsJoined(by: "\n")

        if targetText.length == 0 && replacedContent.count == 0 {
            replacedContent = listPrefix
        }
        
        saveCurrentStateAndRegisterForUndo(targetRange, withAttributedString: NSAttributedString(string: replacedContent, attributes: defaultAttributesForLoad), oldAttributedString: NSAttributedString(string: String(targetText), attributes: defaultAttributesForLoad), selectedRangeLocationMove: replacedContent.count - targetText.length)

        if isCurrentList {
            let listPrefixString: NSString = NSString(string: objectLineAndIndex.0.components(separatedBy: " ")[0]).appending(" ") as NSString
            resetCurrentIndentAndRegisterForUndo(NSRange(location: targetRange.location, length: replacedContent.count), headIndent: listPrefixString.size(withAttributes: [NSAttributedString.Key.font: FontType.shared.regular]).width)
        }

        if isTransformToList {
            let listPrefixString = NSString(string: listPrefix)
            saveCurrentIndentAndRegisterForUndo(NSRange(location: targetRange.location, length: replacedContent.count), headIndent: listPrefixString.size(withAttributes: [NSAttributedString.Key.font: FontType.shared.regular]).width)
        }

    }
    
    func currentParagraphStyle() -> ParagraphStyle {
        return currentParagraphTypeWithLocation(selectedRange.location)
    }

    func currentFontWithIndex(_ index: Int) -> UIFont {
        guard let currentFont = storage.safeAttribute(NSAttributedString.Key.font.rawValue, atIndex: index, effectiveRange: nil, defaultValue: nil) as? UIFont else {
            return FontType.shared.regular
        }
        
        if currentFont.isBold {
            return FontType.shared.bold
        } else if currentFont.isItalic {
            return FontType.shared.italic
        } else if currentFont.isBoldItalic {
            return FontType.shared.boldItalic
        } else {
            return FontType.shared.regular
        }
    }
    
    func defaultParagraphStyle() -> NSMutableParagraphStyle {
        var paragraphStyle: NSMutableParagraphStyle!

        if let defaultParagraphStyle = defaultAttributesForLoad[NSAttributedString.Key.paragraphStyle] as? NSParagraphStyle {
            paragraphStyle = defaultParagraphStyle.mutableCopy() as? NSMutableParagraphStyle
        } else {
            paragraphStyle = NSMutableParagraphStyle()
        }

        return paragraphStyle
    }
}
