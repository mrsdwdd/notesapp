//
//  AlphaTextStorage+Utils.swift.swift
//  AlphaTextView
//
//  Created by Marsudi Widodo on 03/11/21.
//

import UIKit

extension AlphaTextView: AlphaTextStorageDelegate {
    
    func replace(in range: NSRange, with str: String) {
        var listItemFillText: NSString = ""
        var listPrefixItemLength = 0
        var deleteCurrentListPrefixItemByReturn = false
        var deleteCurrentListPrefixItemByBackspace = false
        
        if isReturn(str) {
            
            let objectLineAndIndex = objectLineAndIndexWithString(storage.string, location: range.location)
            let objectLine = objectLineAndIndex.0
            let objectIndex = objectLineAndIndex.1

            switch paragraphTypeWithObjectLine(objectLine) {
            case .ol:
                var number = Int(objectLine.components(separatedBy: ".")[0])
                if number == nil {
                    break
                }
                listPrefixItemLength = "\(number!). ".count
                
                number! += 1
                listItemFillText = "\(number!). " as NSString
                break
            case .ul:
                let listPrefixItem = objectLine.components(separatedBy: " ")[0]
                listItemFillText = "\(listPrefixItem) " as NSString

                listPrefixItemLength = listItemFillText.length

                break
            default:
                break
            }

            let separateds = objectLine.components(separatedBy: " ")
            if separateds.count >= 2 {
                let objectLineRange = NSRange(location: 0, length: objectLine.count)
                if separateds[1] == "" && (ulRegex.matches(in: objectLine, options: .reportProgress, range: objectLineRange).count > 0 || olRegex.matches(in: objectLine, options: .reportProgress, range: objectLineRange).count > 0) {

                    let lastIndex = objectIndex + objectLine.count
                    let isEndOfText = lastIndex >= storage.string.count
                    var isReturnAtNext = false

                    if !isEndOfText {
                        isReturnAtNext = isReturn(NSString(string: storage.string).substring(with: NSRange(location: lastIndex, length: 1)))
                    }

                    if isEndOfText || isReturnAtNext {
                        deleteCurrentListPrefixItemByReturn = true
                        listPrefixItemLength = listItemFillText.length
                        listItemFillText = ""
                    }
                }
            }
        } else if isBackspace(str) && range.length == 1 {
            var firstLine = objectLineWithString(text, location: range.location)
            firstLine.append(" ")

            let separates = firstLine.components(separatedBy: " ").count
            let firstLineRange = NSRange(location: 0, length: firstLine.count)

            if separates == 2 && (ulRegex.matches(in: firstLine, options: .reportProgress, range: firstLineRange).count > 0 || olRegex.matches(in: firstLine, options: .reportProgress, range: firstLineRange).count > 0) {
                deleteCurrentListPrefixItemByBackspace = true
                listPrefixItemLength = firstLineRange.length - 1
                listItemFillText = ""
            }
        }

        storage.isChangeCharacters = true

        storage.beginEditing()

        let finalStr: NSString = "\(str)" as NSString

        storage.backedString.replaceCharacters(in: range, with: String(finalStr))

        storage.edited(.editedCharacters, range: range, changeInLength: (finalStr.length - range.length))

        storage.endEditing()

        if undoManager!.isRedoing {
            return
        }

        if deleteCurrentListPrefixItemByReturn {
            let deleteLocation = range.location - listPrefixItemLength
            let deleteRange = NSRange(location: deleteLocation, length: listPrefixItemLength + 1)
            let deleteString = NSString(string: storage.string).substring(with: deleteRange)

            saveCurrentStateAndRegisterForUndo(deleteRange, withAttributedString: NSAttributedString(), oldAttributedString: NSAttributedString(string: deleteString), selectedRangeLocationMove: -(deleteLocation > 0 ? listPrefixItemLength + 1 : listPrefixItemLength))
            
        } else if deleteCurrentListPrefixItemByBackspace {
            let deleteLocation = range.location - listPrefixItemLength
            let deleteRange = NSRange(location: deleteLocation, length: listPrefixItemLength)
            let deleteString = NSString(string: storage.string).substring(with: deleteRange)

            saveCurrentStateAndRegisterForUndo(deleteRange, withAttributedString: NSAttributedString(), oldAttributedString: NSAttributedString(string: deleteString), selectedRangeLocationMove: -listPrefixItemLength)
        } else {
            let listItemTextLength = listItemFillText.length

            if listItemTextLength > 0 {
                saveCurrentStateAndRegisterForUndo(NSRange(location: range.location + str.count, length: 0), withString: String(listItemFillText), selectedRangeLocationMove: listItemTextLength)
            }
        }
    }
    
    func currentParagraphTypeWithLocation(_ location: Int) -> AlphaTextView.ParagraphStyle {
        if text == "" {
            return .default
        }
        let paragraphRange = paragraphRangeOfString(storage.string, location: location)
        let objectLine = NSString(string: storage.string).substring(with: paragraphRange)
        
        return paragraphTypeWithObjectLine(objectLine)
    }

    func performReplacementsForRange(_ range: NSRange, font: UIFont) {
        if range.length > 0 {
            var attrs: [NSAttributedString.Key: AnyObject] = [NSAttributedString.Key.font : font]
            if storage.underline {
                attrs[NSAttributedString.Key.underlineStyle] = NSUnderlineStyle.single.rawValue as AnyObject?
            } else {
                storage.safeRemoveAttribute(NSAttributedString.Key.underlineStyle, range: range)
            }
            storage.safeAddAttributes(attrs, range: range)
        }
    }
}
