//
//  AlphaTextView+UITextViewDelegate.swift
//  AlphaTextView
//
//  Created by Marsudi Widodo on 03/11/21.
//
//

import UIKit

extension AlphaTextView: UITextViewDelegate {
    
    public func textViewDidChange(_ textView: UITextView) {
        let paragraphType = currentParagraphStyle()

        let objectIndex = objectLineAndIndexWithString(text, location: selectedRange.location).1

        if objectIndex >= text.count || objectIndex < 0 {
            return
        }

        guard let currentParagraphStyle = storage.safeAttribute(NSAttributedString.Key.paragraphStyle.rawValue, atIndex: objectIndex, effectiveRange: nil, defaultValue: nil) as? NSParagraphStyle else {
            return
        }

        var paragraphStyle: NSMutableParagraphStyle? = nil

        if paragraphType == .default {
            if currentParagraphStyle.firstLineHeadIndent == 0 {
                return
            }

            paragraphStyle = defaultParagraphStyle()
            paragraphStyle!.headIndent = 0
            paragraphStyle!.firstLineHeadIndent = 0

        } else if paragraphType == .ol || paragraphType == .ul {
            if currentParagraphStyle.firstLineHeadIndent != 0 {
                return
            }

            let objectLineAndIndex = objectLineAndIndexWithString(self.text, location: selectedRange.location)
            let listPrefixString: NSString = NSString(string: objectLineAndIndex.0.components(separatedBy: " ")[0]).appending(" ") as NSString

            paragraphStyle = defaultParagraphStyle()
            paragraphStyle!.headIndent = FontType.shared.regular.lineHeight + listPrefixString.size(withAttributes: [NSAttributedString.Key.font: FontType.shared.regular]).width
            paragraphStyle!.firstLineHeadIndent = FontType.shared.regular.lineHeight
        }

        if paragraphStyle != nil {
            var defaultAttributes = defaultAttributesForLoad
            defaultAttributes[NSAttributedString.Key.paragraphStyle] = paragraphStyle
            
            let paragraphRange = paragraphRangeOfString(self.text, location: selectedRange.location)
            self.textStorage.addAttributes(defaultAttributes, range: paragraphRange)
            
            var attributes = [NSAttributedString.Key: AnyObject]()
            for key in defaultAttributes.keys {
                attributes[key] = defaultAttributes[key]
            }
            typingAttributes = attributes
        }
        
    }
    
}
