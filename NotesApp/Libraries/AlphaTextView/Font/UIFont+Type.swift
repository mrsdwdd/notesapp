//
//  UIFont+Type.swift
//  AlphaTextView
//
//  Created by Marsudi Widodo on 03/11/21.
//

import UIKit

class FontType {
    
    static let shared = FontType()
    
    var regular: UIFont {
        return UIFont.systemFont(ofSize: 14)
    }
    
    var bold: UIFont {
        return UIFont.systemFont(ofSize: 14, weight: .black)
    }
    
    var italic: UIFont {
        return UIFont.italicSystemFont(ofSize: 14)
    }
    
    var boldItalic: UIFont {
        return UIFont.italicSystemFont(ofSize: 14, weight: .black)
    }
}

extension UIFont {
    
    var isRegular:  Bool {
        let symbolicTraits = self.fontDescriptor.symbolicTraits
        
        if symbolicTraits.contains(.traitBold) || symbolicTraits.contains(.traitItalic) {
            return false
        }
        
        return true
    }
    
    var isBold:  Bool {
        let symbolicTraits = self.fontDescriptor.symbolicTraits
        
        if symbolicTraits.contains(.traitBold), !symbolicTraits.contains(.traitItalic) {
            return true
        }
        
        return false
    }
    
    var isItalic:  Bool {
        let symbolicTraits = self.fontDescriptor.symbolicTraits
        
        if !symbolicTraits.contains(.traitBold), symbolicTraits.contains(.traitItalic) {
            return true
        }
        
        return false
    }
    
    var isBoldItalic:  Bool {
        let symbolicTraits = self.fontDescriptor.symbolicTraits
        
        if symbolicTraits.contains(.traitBold), symbolicTraits.contains(.traitItalic) {
            return true
        }
        
        return false
    }
}
