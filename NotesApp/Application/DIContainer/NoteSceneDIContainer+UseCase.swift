//
//  NoteSceneDIContainer+UseCase.swift
//  NotesApp
//
//  Created by Marsudi Widodo on 01/11/21.
//

import Foundation

extension NotesSceneDIContainer {
    // MARK: - Use Cases
    func makeFetchNotesUseCase(requestValue: FetchNotesUseCase.RequestValue,
                               completion: @escaping (FetchNotesUseCase.ResultValue) -> Void) -> UseCase {
        return FetchNotesUseCase(requestValue: requestValue,
                                 completion: completion,
                                 noteRepository: makeNoteRepository())
    }
    
    func makeAddNotesUseCase(requestValue: AddNotesUseCase.RequestValue,
                                    completion: @escaping (AddNotesUseCase.ResultValue) -> Void) -> UseCase {
        return AddNotesUseCase(requestValue: requestValue,
                               completion: completion,
                               noteRepository: makeNoteRepository())
    }
    
    func makeEditNotesUseCase(requestValue: EditNotesUseCase.RequestValue,
                                    completion: @escaping (EditNotesUseCase.ResultValue) -> Void) -> UseCase {
        return EditNotesUseCase(requestValue: requestValue,
                               completion: completion,
                               noteRepository: makeNoteRepository())
    }
    
    func makeDeleteNotesUseCase(requestValue: DeleteNoteUseCase.RequestValue,
                                    completion: @escaping (DeleteNoteUseCase.ResultValue) -> Void) -> UseCase {
        return DeleteNoteUseCase(requestValue: requestValue,
                               completion: completion,
                               noteRepository: makeNoteRepository())
    }
}
