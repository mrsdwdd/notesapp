//
//  AppDIContainer.swift
//  NotesApp
//
//  Created by Marsudi Widodo on 31/10/21.
//

import Foundation

final class AppDIContainer {
    
    // MARK: - DIContainers of scenes
    func makeNotesSceneDIContainer() -> NotesSceneDIContainer {
        return NotesSceneDIContainer()
    }
}
