//
//  NotesSceneDIContainer.swift
//  NotesApp
//
//  Created by Marsudi Widodo on 31/10/21.
//

import UIKit
import SwiftUI

final class NotesSceneDIContainer {
    
    // MARK: - Persistent Storage
    lazy var notesStorage: NotesStorage = CoreDataNotesStorage(maxStorageLimit: 10)
    
    // MARK: - Repositories
    func makeNoteRepository() -> NoteRepository {
        return DefaultNoteRepository(notePersistentStorage: notesStorage)
    }
    
    // MARK: - Notes List
    func makeNotesListViewController(actions: NotesListViewModelActions) -> NotesListViewController {
        return NotesListViewController.create(with: makeNotesListViewModel(actios: actions))
    }
    
    func makeNotesListViewModel(actios: NotesListViewModelActions) -> NotesListViewModel {
        return DefaultNotesListViewModel(fetchNoteUseCaseFactory: makeFetchNotesUseCase, actions: actios)
    }
    
    func makeEditNotesEditorViewController(actions: NotesEditorViewModelActions, note: Note) -> NotesEditorViewController {
        return NotesEditorViewController.create(with: makeEditNotesEditorViewModel(actions: actions, note: note))
    }
    
    func makeEditNotesEditorViewModel(actions: NotesEditorViewModelActions, note: Note) -> NotesEditorViewModel {
        return DefaultNotesEditorViewModel(addNoteUseCaseFactory: makeAddNotesUseCase, editNoteUseCaseFactory: makeEditNotesUseCase, deleteNoteUseCaseFactory: makeDeleteNotesUseCase, actions: actions, note: note)
    }
    
    func makeNewNotesEditorViewController(actions: NotesEditorViewModelActions) -> NotesEditorViewController {
        return NotesEditorViewController.create(with: makeNotesEditorViewModel(actions: actions))
    }
    
    func makeNotesEditorViewModel(actions: NotesEditorViewModelActions) -> NotesEditorViewModel {
        return DefaultNotesEditorViewModel(addNoteUseCaseFactory: makeAddNotesUseCase, editNoteUseCaseFactory: makeEditNotesUseCase, deleteNoteUseCaseFactory: makeDeleteNotesUseCase, actions: actions)
    }
    
    // MARK: - Flow Coordinators
    func makeNotesFlowCoordinator(navigationController: UINavigationController) -> NotesFlowCoordinator {
        return NotesFlowCoordinator(navigationController: navigationController, dependencies: self)
    }
}

extension NotesSceneDIContainer: NotesFlowCoordinatorDependencies {}
