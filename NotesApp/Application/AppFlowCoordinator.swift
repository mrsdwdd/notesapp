//
//  AppFlowCoordinator.swift
//  NotesApp
//
//  Created by Marsudi Widodo on 31/10/21.
//

import UIKit

final class AppFlowCoordinator {

    var navigationController: UINavigationController
    private let appDIContainer: AppDIContainer
    
    init(navigationController: UINavigationController,
         appDIContainer: AppDIContainer) {
        self.navigationController = navigationController
        self.appDIContainer = appDIContainer
    }
    
    func start() {
        let noteSceneDIContainer = appDIContainer.makeNotesSceneDIContainer()
        let flow = noteSceneDIContainer.makeNotesFlowCoordinator(navigationController: navigationController)
        flow.start()
    }
}
