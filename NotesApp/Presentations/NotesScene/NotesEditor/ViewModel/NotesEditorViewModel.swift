//
//  NotesEditorViewModel.swift
//  NotesApp
//
//  Created by Marsudi Widodo on 31/10/21.
//

import Foundation
import UIKit

struct NotesEditorViewModelActions {
    let popViewController: (_ viewController: UIViewController) -> Void
    let updateList: () -> Void
}

protocol NotesEditorViewModelInput {
    func viewDidLoad()
    func didSave(title: String, content: String, color: NoteColor, completion: @escaping () -> Void)
    func didDelete(completion: @escaping () -> Void)
    func popViewController(viewController: UIViewController)
    func updateSelectedColor(color: NoteColor)
}

protocol NotesEditorViewModelOutput {
    var id: UUID { get }
    var title: String { get }
    var content: String { get }
    var date: Date { get }
    var selectedColor: Observable<NoteColor> { get }
    var isEdit: Observable<Bool> { get }
}

enum NoteColor {
    case blue, orange, green, red
}

protocol NotesEditorViewModel: NotesEditorViewModelInput, NotesEditorViewModelOutput {}

typealias AddNotesUseCaseFactory = (
    AddNotesUseCase.RequestValue,
    @escaping (AddNotesUseCase.ResultValue) -> Void
    ) -> UseCase

typealias EditNotesUseCaseFactory = (
    EditNotesUseCase.RequestValue,
    @escaping (EditNotesUseCase.ResultValue) -> Void
    ) -> UseCase

typealias DeleteNotesUseCaseFactory = (
    DeleteNoteUseCase.RequestValue,
    @escaping (DeleteNoteUseCase.ResultValue) -> Void
    ) -> UseCase

final class DefaultNotesEditorViewModel: NotesEditorViewModel {
    
    private let addNoteUseCaseFactory: AddNotesUseCaseFactory
    private let editNoteUseCaseFactory: EditNotesUseCaseFactory
    private let deleteNoteUseCaseFactory: DeleteNotesUseCaseFactory
    private let actions: NotesEditorViewModelActions?
    
    // MARK: - OUTPUT
    let id: UUID
    let title: String
    let content: String
    let date: Date
    let selectedColor: Observable<NoteColor> = Observable(.blue)
    var isEdit: Observable<Bool> = Observable(false)
    var note: Note?

    // MARK: - Init
    init(addNoteUseCaseFactory: @escaping AddNotesUseCaseFactory,
         editNoteUseCaseFactory: @escaping EditNotesUseCaseFactory,
         deleteNoteUseCaseFactory: @escaping DeleteNotesUseCaseFactory,
         actions: NotesEditorViewModelActions? = nil, note: Note? = nil) {
        
        self.addNoteUseCaseFactory = addNoteUseCaseFactory
        self.editNoteUseCaseFactory = editNoteUseCaseFactory
        self.deleteNoteUseCaseFactory = deleteNoteUseCaseFactory
        
        self.actions = actions
        self.id = note?.id ?? UUID()
        self.title = note?.title ?? ""
        self.content = note?.content ?? ""
        self.date = note?.createdAt ?? Date()
        
        var color: NoteColor = .blue
        switch note?.color ?? "#0000ff" {
        case "#0000ff":
            color = .blue
        case "#ffa500":
            color = .orange
        case "#00ff00":
            color = .green
        case "#ff0000":
            color = .red
        default: color = .blue
        }
        self.selectedColor.value = color
        self.isEdit.value = note != nil
        self.note = note
    }

    // MARK: - Private
    func save(title: String, content: String, color: NoteColor, completion: @escaping () -> Void) {
        var _color: UIColor = .blue
        switch color {
        case .blue:
            _color = .blue
        case .orange:
            _color = .orange
        case .green:
            _color = .green
        case .red:
            _color = .red
        }
        let note = Note(id: UUID(), title: title, content: content, createdAt: Date(), color: _color.toHexString())
        let request = AddNotesUseCase.RequestValue(note: note)
        let useCase = addNoteUseCaseFactory(request) { _ in
            completion()
        }
        
        useCase.save?()
    }
    
    func update(title: String, content: String, color: NoteColor, completion: @escaping () -> Void) {
        var _color: UIColor = .blue
        switch color {
        case .blue:
            _color = .blue
        case .orange:
            _color = .orange
        case .green:
            _color = .green
        case .red:
            _color = .red
        }
        let note = Note(id: id, title: title, content: content, createdAt: Date(), color: _color.toHexString())
        let request = EditNotesUseCase.RequestValue(note: note)
        let useCase = editNoteUseCaseFactory(request) { _ in
            completion()
        }
        
        useCase.update?()
    }
    
    func delete(completion: @escaping () -> Void) {
        guard let note = note else { return }

        let request = DeleteNoteUseCase.RequestValue(note: note)
        let useCase = deleteNoteUseCaseFactory(request) { _ in
            completion()
        }
        useCase.delete?()
    }
    
}

// MARK: - INPUT. View event methods
extension DefaultNotesEditorViewModel {
    
    func viewDidLoad() { }
    
    func didSave(title: String, content: String, color: NoteColor, completion: @escaping () -> Void) {
        if isEdit.value {
            update(title: title, content: content, color: color, completion: completion)
        } else {
            save(title: title, content: content, color: color, completion: completion)
        }
    }
    
    func didDelete(completion: @escaping () -> Void) {
        delete(completion: completion)
    }
    
    func popViewController(viewController: UIViewController) {
        actions?.updateList()
        actions?.popViewController(viewController)
    }
    
    func updateSelectedColor(color: NoteColor) {
        selectedColor.value = color
    }
}
