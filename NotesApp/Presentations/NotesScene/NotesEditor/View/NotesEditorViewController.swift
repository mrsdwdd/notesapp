//
//  NotesEditorViewController.swift
//  NotesApp
//
//  Created by Marsudi Widodo on 31/10/21.
//

import UIKit

class NotesEditorViewController: UIViewController {
    
    private var viewModel: NotesEditorViewModel!
    
    @IBOutlet weak var blueImageView: UIImageView!
    @IBOutlet weak var orangeImageView: UIImageView!
    @IBOutlet weak var greenImageView: UIImageView!
    @IBOutlet weak var redImageView: UIImageView!
    @IBOutlet weak var textViewContainer: UIView!
    
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var dateLabel: UILabel!
    
    var contentTextView: AlphaTextView!
    
    // MARK: - Lifecycle

    static func create(with viewModel: NotesEditorViewModel) -> NotesEditorViewController {
        let view = NotesEditorViewController()
        view.viewModel = viewModel
        return view
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.accessibilityIdentifier = "notesEditorMainView"
        
        hideKeyboardWhenTappedAround()
        setupHandle()
        setupViews()
        bind(to: viewModel)
    }
    
    func bind(to viewModel: NotesEditorViewModel) {
        viewModel.isEdit.observe(on: self) { [weak self] status in
            self?.setupBarButtonItems(isEdit: status)
            if !status {
                self?.titleTextField.becomeFirstResponder()
            }
        }
    }
    
    func setupHandle() {
        viewModel.selectedColor.observe(on: self) { [unowned self] color in
            blueImageView.image = color == .blue ? UIImage(systemName: "checkmark.circle.fill") : UIImage(systemName: "circle.inset.filled")
            orangeImageView.image = color == .orange ? UIImage(systemName: "checkmark.circle.fill") : UIImage(systemName: "circle.inset.filled")
            greenImageView.image = color == .green ? UIImage(systemName: "checkmark.circle.fill") : UIImage(systemName: "circle.inset.filled")
            redImageView.image = color == .red ? UIImage(systemName: "checkmark.circle.fill") : UIImage(systemName: "circle.inset.filled")
        }
        
        blueImageView.addTapGestureRecognizer { [unowned self] in
            viewModel.updateSelectedColor(color: .blue)
        }
        
        orangeImageView.isUserInteractionEnabled = true
        orangeImageView.addTapGestureRecognizer { [unowned self] in
            viewModel.updateSelectedColor(color: .orange)
        }
        
        greenImageView.addTapGestureRecognizer { [unowned self] in
            viewModel.updateSelectedColor(color: .green)
        }
        
        redImageView.addTapGestureRecognizer { [unowned self] in
            viewModel.updateSelectedColor(color: .red)
        }
        
    }
    
    func setupBarButtonItems(isEdit: Bool) {
        let saveBarButtonItem = UIBarButtonItem(image: UIImage(systemName: "checkmark"), style: .plain, target: self, action: #selector(saveNote))
        saveBarButtonItem.accessibilityIdentifier = "saveBarButtonItem"
        let deleteBarButtonItem = UIBarButtonItem(image: UIImage(systemName: "multiply")?.withTintColor(.red), style: .plain, target: self, action: #selector(deleteNote))
        deleteBarButtonItem.accessibilityIdentifier = "deleteBarButtonItem"
        
        if isEdit {
            navigationItem.rightBarButtonItems = [saveBarButtonItem, deleteBarButtonItem]
        } else {
            navigationItem.rightBarButtonItems = [saveBarButtonItem]
        }
    }
    
    func setupViews() {
        contentTextView = AlphaTextView(frame: CGRect(x: 10,
                                                      y: 0,
                                                      width: UIScreen.main.bounds.size.width - 20,
                                                      height: self.textViewContainer.bounds.size.height - 40))
        textViewContainer.addSubview(contentTextView)
        
        titleTextField.text = viewModel.title
        contentTextView.setAttributeTextWithJSONString(viewModel.content)
        
        contentTextView.accessibilityIdentifier = "contentTextView"
        dateLabel.text = viewModel.date.formatted()
        
        blueImageView.image = viewModel.selectedColor.value == .blue ? UIImage(systemName: "checkmark.circle.fill") : UIImage(systemName: "circle.inset.filled")
        blueImageView.accessibilityIdentifier = "blueImageView"
        
        orangeImageView.image = viewModel.selectedColor.value == .orange ? UIImage(systemName: "checkmark.circle.fill") : UIImage(systemName: "circle.inset.filled")
        orangeImageView.accessibilityIdentifier = "orangeImageView"
        
        greenImageView.image = viewModel.selectedColor.value == .green ? UIImage(systemName: "checkmark.circle.fill") : UIImage(systemName: "circle.inset.filled")
        greenImageView.accessibilityIdentifier = "greenImageView"
        
        redImageView.image = viewModel.selectedColor.value == .red ? UIImage(systemName: "checkmark.circle.fill") : UIImage(systemName: "circle.inset.filled")
        redImageView.accessibilityIdentifier = "redImageView"
    }
    
    @objc func saveNote() {
        let noteTitle = titleTextField.text ?? "Untitled"
        viewModel.didSave(title: noteTitle, content: contentTextView.textAttributesJSON(), color: viewModel.selectedColor.value) { [weak self] in
            guard let self = self else { return }
            DispatchQueue.main.async {
                self.viewModel.popViewController(viewController: self)
            }
        }
    }
    
    @objc func deleteNote() {
        let alert = UIAlertController(title: "Delete Note", message: "Are you sure want to delete this note?", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Yes, I am sure", style: .default, handler: performDeleteNote(_:)))
        alert.addAction(UIAlertAction(title: "I think no", style: .cancel, handler: nil))

        self.present(alert, animated: true)
    }
    
    func performDeleteNote(_ action: UIAlertAction) {
        self.viewModel.didDelete(completion: handleAfterDelete)
    }
    
    func handleAfterDelete() {
        DispatchQueue.main.async {
            self.viewModel.popViewController(viewController: self)
        }
    }
}
