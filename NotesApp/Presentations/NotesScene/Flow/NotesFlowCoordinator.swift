//
//  NotesFlowCoordinator.swift
//  NotesApp
//
//  Created by Marsudi Widodo on 31/10/21.
//

import UIKit

protocol NotesFlowCoordinatorDependencies  {
    func makeNotesListViewController(actions: NotesListViewModelActions) -> NotesListViewController
    func makeEditNotesEditorViewController(actions: NotesEditorViewModelActions, note: Note) -> NotesEditorViewController
    func makeNewNotesEditorViewController(actions: NotesEditorViewModelActions) -> NotesEditorViewController
}

final class NotesFlowCoordinator {
    
    private weak var navigationController: UINavigationController?
    private let dependencies: NotesFlowCoordinatorDependencies

    private weak var moviesListVC: NotesListViewController?

    init(navigationController: UINavigationController,
         dependencies: NotesFlowCoordinatorDependencies) {
        self.navigationController = navigationController
        self.dependencies = dependencies
    }
    
    func start() {
        let actions = NotesListViewModelActions(gotoEditNoteEditor: gotoEditNoteEditor, goNewNoteEditor: goNewNoteEditor)
        let vc = dependencies.makeNotesListViewController(actions: actions)
        navigationController?.viewControllers = [vc]
        moviesListVC = vc
    }
    
    private func gotoEditNoteEditor(note: Note, updateList: @escaping () -> Void) {
        let actions = NotesEditorViewModelActions(popViewController: popViewController, updateList: updateList)
        let vc = dependencies.makeEditNotesEditorViewController(actions: actions, note: note)
        navigationController?.pushViewController(vc, animated: true)
    }
    
    private func goNewNoteEditor(updateList: @escaping () -> Void) {
        let actions = NotesEditorViewModelActions(popViewController: popViewController, updateList: updateList)
        let vc = dependencies.makeNewNotesEditorViewController(actions: actions)
        navigationController?.pushViewController(vc, animated: true)
    }
    
    private func popViewController(_ viewController: UIViewController) {
        viewController.navigationController?.popViewController(animated: true)
    }
    
}
