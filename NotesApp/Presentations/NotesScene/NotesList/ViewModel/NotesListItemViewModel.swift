//
//  NotesListItemViewModel.swift
//  NotesApp
//
//  Created by Marsudi Widodo on 31/10/21.
//

import Foundation
import UIKit

struct NotesListItemViewModel: Equatable {
    let title: String?
    let content: String?
    let createdAt: String?
    let color: UIColor?
}

extension NotesListItemViewModel {
    
    init(note: Note) {
        self.title = note.title
        self.content = note.content
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "d MMM, y"
        self.createdAt = (note.createdAt != nil) ? "" : dateFormatter.string(from: note.createdAt!)
        self.color = UIColor(hex: note.color ?? "#000000")
    }
}
