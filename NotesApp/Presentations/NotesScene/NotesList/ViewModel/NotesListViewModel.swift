//
//  NotesListViewModel.swift
//  NotesApp
//
//  Created by Marsudi Widodo on 31/10/21.
//

import Foundation

struct NotesListViewModelActions {
    let gotoEditNoteEditor: (Note, @escaping () -> Void) -> Void
    let goNewNoteEditor: (@escaping () -> Void) -> Void
}

protocol NotesListViewModelInput {
    func viewDidLoad()
    func didSelectItem(at index: Int)
    func goNewNoteEditor()
}

protocol NotesListViewModelOutput {
    var items: Observable<[NotesListItemViewModel]> { get }
    var title: String { get }
    var count: Observable<Int> { get }
}

protocol NotesListViewModel: NotesListViewModelInput, NotesListViewModelOutput { }

typealias FetchNotesUseCaseFactory = (
    FetchNotesUseCase.RequestValue,
    @escaping (FetchNotesUseCase.ResultValue) -> Void
    ) -> UseCase

final class DefaultNotesListViewModel: NotesListViewModel {
    
    private let fetchNoteUseCaseFactory: FetchNotesUseCaseFactory
    private let actions: NotesListViewModelActions?
    private var notes: [Note] = []
    
    // MARK: - OUTPUT
    let items: Observable<[NotesListItemViewModel]> = Observable([])
    let title: String = "Notes"
    let count: Observable<Int> = Observable(0)

    // MARK: - Init
    init(fetchNoteUseCaseFactory: @escaping FetchNotesUseCaseFactory, actions: NotesListViewModelActions? = nil) {
        self.fetchNoteUseCaseFactory = fetchNoteUseCaseFactory
        self.actions = actions
    }
    
    // MARK: - Private
    private func load() {
        let request = FetchNotesUseCase.RequestValue(maxCount: 100)
        let completion: (FetchNotesUseCase.ResultValue) -> Void = { result in
            switch result {
            case .success(let items):
                print(items.count)
                self.notes = items
                self.items.value = items.map(NotesListItemViewModel.init)
                self.count.value = items.count
            case .failure: break
            }
        }
        
        let useCase = fetchNoteUseCaseFactory(request, completion)
        useCase.fetch?()
    }
}

// MARK: - INPUT. View event methods
extension DefaultNotesListViewModel {

    func viewDidLoad() {
        load()
    }
    
    func didSelectItem(at index: Int) {
        actions?.gotoEditNoteEditor(notes[index], load)
    }
    
    func goNewNoteEditor() {
        actions?.goNewNoteEditor(load)
    }
}
