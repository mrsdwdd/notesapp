//
//  NotesListViewController.swift
//  NotesApp
//
//  Created by Marsudi Widodo on 31/10/21.
//

import UIKit

class NotesListViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var countLabel: UILabel!
    
    private var viewModel: NotesListViewModel!
    
    // MARK: - Lifecycle

    static func create(with viewModel: NotesListViewModel) -> NotesListViewController {
        let view = NotesListViewController()
        view.viewModel = viewModel
        return view
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
        bind(to: viewModel)
        viewModel.viewDidLoad()
    }
    
    @IBAction func handleNewNoteButton(_ sender: UIButton) {
        viewModel.goNewNoteEditor()
    }
    
    // MARK: - PRIVATE
    private func setupViews() {
        title = viewModel.title
        tableView.register(UINib(nibName: NotesListItemCell.reuseIdentifier, bundle: nil), forCellReuseIdentifier: NotesListItemCell.reuseIdentifier)
        tableView.estimatedRowHeight = NotesListItemCell.height
        tableView.rowHeight = UITableView.automaticDimension
    }
    
    private func bind(to viewModel: NotesListViewModel) {
        viewModel.items.observe(on: self) { [weak self] _ in
            self?.updateItems()
        }
        
        viewModel.count.observe(on: self) { [weak self] count in
            self?.updateCountLabel(count: count)
        }
    }
    
    private func updateItems() {
        tableView.reloadData()
    }
    
    private func updateCountLabel(count: Int) {
        countLabel.text = count == 0 ? "You have note notes here. . ." : "You have \(count) note(s)"
    }
    
}

extension NotesListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.items.value.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(
            withIdentifier: NotesListItemCell.reuseIdentifier,
            for: indexPath) as? NotesListItemCell else {
            return UITableViewCell()
        }

        cell.update(with: viewModel.items.value[indexPath.row])

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.didSelectItem(at: indexPath.row)
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
