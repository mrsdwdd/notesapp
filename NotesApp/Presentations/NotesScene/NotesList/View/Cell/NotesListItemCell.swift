//
//  NoteListitemCell.swift
//  NotesApp
//
//  Created by Marsudi Widodo on 31/10/21.
//

import UIKit

final class NotesListItemCell: UITableViewCell {

    static let reuseIdentifier = String(describing: NotesListItemCell.self)
    static let height = CGFloat(80)
    
    @IBOutlet private var titleLabel: UILabel!
    @IBOutlet private var dateLabel: UILabel!
    @IBOutlet private var contentLabel: UILabel!
    @IBOutlet private var colorView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func update(with viewModel: NotesListItemViewModel) {
        self.titleLabel.text = viewModel.title
        self.dateLabel.text = viewModel.createdAt
        self.colorView.backgroundColor = viewModel.color
        
            
        guard let data = viewModel.content?.data(using: .utf8) else {
            return
        }
        
        let jsonDict = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: AnyObject]
        let text = jsonDict?["text"] as? String
        self.contentLabel.text = text ?? ""
        
    }
}

