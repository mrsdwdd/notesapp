//
//  AlphaView.swift
//  NotesApp
//
//  Created by Marsudi Widodo on 31/10/21.
//

import UIKit

@IBDesignable
open class AlphaView: UIView {
    
    open var borderSetted = false
    open var shadowSetted = false
    
    enum cornerSide: String {
        case top
        case left
        case bottom
        case right
    }
    
    @IBInspectable var alphaBorderWidth: CGFloat = 0.0 {
        didSet{
            self.borderSetted = true
            setup()
        }
    }
    @IBInspectable var alphaCornerRadius: CGFloat = 0.0{
        didSet{
            setup()
        }
    }
    
    
    @IBInspectable var alphaCornerRadiusSide: String = "" {
        didSet{
            self.clipsToBounds = true
            
            let side = cornerSide(rawValue: self.alphaCornerRadiusSide.lowercased())
            layer.cornerRadius = self.alphaCornerRadius
            
            switch side {
            case .top:
                layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
                
            case .left:
                layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMinYCorner]
                
            case .bottom:
                layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
                
            case .right:
                layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMaxYCorner]
                
            case .none:
                break;
            }
            
        }
    }
    
    
    @IBInspectable var alphaBorderColor: UIColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1) {
        didSet{
            setup()
        }
    }
    @IBInspectable var alphaShadowColor: UIColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1) {
        didSet{
            self.shadowSetted = true
            setup()
        }
    }
    @IBInspectable var alphaShadowRadius: CGFloat = 0.0 {
        didSet{
            setup()
        }
    }
    @IBInspectable var alphaShadowOpacity: Float = 1.0 {
        didSet{
            setup()
        }
    }
    @IBInspectable var alphaShadowOffset: CGSize = CGSize(width: 0.0, height: 2.0) {
        didSet{
            setup()
        }
    }
    
    func setup() {
        self.layer.masksToBounds = false
        if self.borderSetted{
            layer.borderWidth = alphaBorderWidth
            layer.borderColor = alphaBorderColor.cgColor
        }
        if self.shadowSetted{
            layer.shadowColor = alphaShadowColor.cgColor
            layer.shadowRadius = alphaShadowRadius
            layer.shadowOpacity = alphaShadowOpacity
            layer.shadowOffset = alphaShadowOffset
            layoutIfNeeded()
            layer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: layer.cornerRadius).cgPath
        }
        
        layer.cornerRadius = alphaCornerRadius
    }

    @IBInspectable var dashWidth: CGFloat = 0
    @IBInspectable var dashColor: UIColor = .clear
    @IBInspectable var dashLength: CGFloat = 0
    @IBInspectable var betweenDashesSpace: CGFloat = 0
    
    var dashBorder: CAShapeLayer?
    
    func setDashedBorder() {
        dashBorder?.removeFromSuperlayer()
        let dashBorder = CAShapeLayer()
        dashBorder.lineWidth = dashWidth
        dashBorder.strokeColor = dashColor.cgColor
        dashBorder.lineDashPattern = [dashLength, betweenDashesSpace] as [NSNumber]
        dashBorder.frame = bounds
        dashBorder.fillColor = nil
        if alphaCornerRadius > 0 {
            dashBorder.path = UIBezierPath(roundedRect: bounds, cornerRadius: alphaCornerRadius).cgPath
        } else {
            dashBorder.path = UIBezierPath(rect: bounds).cgPath
        }
        layer.addSublayer(dashBorder)
        self.dashBorder = dashBorder
    }
    
    open override func layoutSubviews() {
        super.layoutSubviews()
        layoutIfNeeded()
        layer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: layer.cornerRadius).cgPath
        setDashedBorder()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
}
